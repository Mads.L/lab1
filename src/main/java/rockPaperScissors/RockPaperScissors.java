package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;


public class RockPaperScissors {
	
    public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    } 

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    Random rand = new Random();
    
    public void run() {
        boolean playing = true;

        while (playing) {
            System.out.printf("Let's play round %d\n", roundCounter);
            int userMove = -1;
            int pcMove = rand.nextInt(3);

            while (true) {
                String input = readInput("Your choice (Rock/Paper/Scissors)?");
                userMove = rpsChoices.indexOf(input);

                if (userMove >=0) {
                    break;
                } else {
                    System.out.printf("I do not understand %s. Could you try again?\n", input);
                }
            }

            System.out.printf("Human chose %s, computer chose %s. ", rpsChoices.get(userMove), rpsChoices.get(pcMove));

            switch (((pcMove - userMove +3) % 3)) {
                case 0:
                    System.out.println("It's a tie!");
                    break;
                case 1:
                    System.out.println("Computer Wins!");
                    computerScore++;
                    break;
                case 2:
                    System.out.println("Human wins!");
                    humanScore++;
                    break;
                default:
                    System.out.println("Something fucked up!");
                    break;
            }
            
            System.out.printf("Score: human %d, computer %d\n", humanScore, computerScore);
            boolean answered = false;

            while(!answered){
                String yesOrNo = readInput("Do you wish to continue playing? (y/n)?");

                switch (yesOrNo) {
                    case "y":
                        roundCounter++;
                        answered = true;
                        break;
                    case "n":
                        System.out.println("Bye bye :)");
                        playing = false;
                        answered = true;
                        break;
                    default:
                        System.out.printf("I do not understand %s. Could you try again?\n", yesOrNo);
                        break;
                }
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}