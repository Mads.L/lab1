package INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        //findLongestWords("aaaa", "aaaaa", "aaaaaaaa");
        //System.out.println(isLeapYear(0));
        //System.out.println(isEvenPositiveInt(0)); 
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        
        int maxLen = word1.length();

        if(maxLen < word2.length()) {maxLen = word2.length();}
        if(maxLen < word3.length()) {maxLen = word3.length();}

        if(word1.length() == maxLen) {System.out.println(word1);}
        if(word2.length() == maxLen) {System.out.println(word2);}
        if(word3.length() == maxLen) {System.out.println(word3);}
    }

    public static boolean isLeapYear(int year) {

        if(((year % 4) == 0)) {
            if((year % 100) == 0) {
                if((year % 400) == 0) {
                    return true;}
                else {return false;}
            } else {return true;}
        } else {return false;}
    }

    public static boolean isEvenPositiveInt(int num) {
        
        if((num >= 0) && ((num % 2) == 0)) {
            return true;} 
        else {
            return false;}
    }
}
