package INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        ArrayList<Integer> mylist = new ArrayList<>(Arrays.asList(1,2,3,4,4));

        System.out.println(multipliedWithTwo(mylist));
        System.out.println(removeThrees(mylist));
        System.out.println(uniqueValues(mylist));
        addList(mylist, mylist);
        System.out.println(mylist);
        
    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        for(int i = 0; i < list.size(); i++){
            list.set(i, list.get(i)*2);
        }
        return list;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        
        for(int i = 0; i < list.size(); i++){
            if(list.get(i) == 3) {
                list.remove(i);
                i--;
            }
        }
        return list;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> uniqueValues = new ArrayList<>();
        uniqueValues.add(list.get(0));

        for(int i = 1; i < list.size(); i++){

            if(!(uniqueValues.contains(list.get(i)))){
                uniqueValues.add(list.get(i));
            } 
        }
        return uniqueValues;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for(int i = 0; i < a.size(); i++){
            a.set(i, (a.get(i) + b.get(i)));
        };
    }

}