package INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {

        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(3, 0, 9)));
        grid1.add(new ArrayList<>(Arrays.asList(4, 5, 3)));
        grid1.add(new ArrayList<>(Arrays.asList(6, 8, 1)));

        //removeRow(grid1, 0);
        //System.out.println(grid1);

        System.out.println(allRowsAndColsAreEqualSum(grid1));
        
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        ArrayList<ArrayList<Integer>> newGrid = new ArrayList<>();

        for(int i = 0; i < grid.size(); i++){
            if(i != row){
                newGrid.add(grid.get(i));
            }
        }

        grid.clear();
        grid.addAll(newGrid);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        boolean result = true;
        
        int firstRowSum = 0;
        int firstColSum = 0;
        
        for(int x = 0; x < grid.size(); x++){
            firstRowSum += grid.get(0).get(x);
        }
        for(int y = 0; y < grid.size(); y++){
            firstColSum += grid.get(y).get(0);

        }

        for(int y = 1; y < grid.size(); y++){
            int diffRowSum = 0;

            for(int x = 0; x < grid.size(); x++){
                diffRowSum += grid.get(y).get(x);
            }

            if(firstRowSum != diffRowSum){result = false;}
        }

        for(int x = 1; x < grid.size(); x++){
            int diffColSum = 0;

            for(int y = 0; y < grid.size(); y++){
                diffColSum += grid.get(y).get(x);
            }

            if(firstColSum != diffColSum){result = false;}
        }

        return result;
    }

}